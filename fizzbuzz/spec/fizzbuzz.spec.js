describe('fizzbuzz', function () {
  xit('does nothing with regular numbers', () => {
    const regularNumber = 2

    const result = fizzbuzz(regularNumber)

    expect(result).toEqual(regularNumber)
  })

  xit('evaluates a number divisible by three as "Fizz"', () => {
    const divisibleNumber = 3

    const result = fizzbuzz(divisibleNumber)

    expect(result).toEqual('Fizz')
  })

  xit('evaluates a number divisible by five as "Buzz"', () => {
    const divisibleNumber = 5

    const result = fizzbuzz(divisibleNumber)

    expect(result).toEqual('Buzz')
  })

  xit('evaluates a number divisible by three and five as "FizzBuzz"', () => {
    const divisibleNumber = 60

    const result = fizzbuzz(divisibleNumber)

    expect(result).toEqual('FizzBuzz')
  })
})
