### Mentorship Katas

Un conjunto de katas en castellano pensadas para facilitar la entrada en _Test Driven Development_ (TDD) a la gente que no ha programado aún.


#### ¿Qué es un Test?

Un test es un bloque de código que estresa nuestro código fuente y comprueba que lo que programamos se comporta como el programador desea. Un test normalmente se compone de cinco partes (sujeto, título, preparación (_arrange_), actuación (_act_) y expectación (_assert_)). Como ejemplo:

```
describe('MiFuncionDeSumar', () => { // nombre del sujeto bajo pruebas
  it('suma dos numeros', () => { // título
    primerNumero = 1  // preparación
    segundoNumero = 3 // preparación
    valorEsperado = 4 // preparación

    resultado = MiFuncionDeSumar(1, 3) // actuación

    expect(resultado).toEqual(valorEsperado) // expectación
    })
})
```

los tests normalmente se lanzan con un comando de terminal, pero en este proyecto usa Jasmine (una librería de terceros que nos da la implementación de «describe», «it», etcétera) y se lanzan en un navegador web. Los tests están escritos por humanos, lo bien nombrados y hechos que estén dependen de la pericia del programador.


#### ¿Cómo usar este repositorio?

En la raíz tenemos la carpeta 'fizzbuzz' con un fichero index.html. Al arrastrarlo a nuestro navegador veremos una lista de los tests en amarillo, esto es porque están desabilitados (_pending_). El flujo de acción sería el siguiente:

* Lanzamos los tests refrescando el navegador
* Habilitamos el primer test de `fizzbuzz/spec/fizzbuzz.spec.js` (Cambiamos 'xit' por 'it' y guardamos el fichero)
* Lanzamos los tests de nuevo y comprobamos que tenemos un solo test en _rojo_
* Implementamos el código necesario más simple que nos podamos imaginar en `fizzbuzz/src/fizzbuzz.js` que haga pasar el test (Podemos lanzar los tests cuantas veces queramos y es recomendable hacerlo después de cada pequeño cambio)
* Cuando consigamos que el test esté verde podemos mejorar la legibilidad del código (sin romper el test que acabamos de pasar)
* Si estamos satisfechos con nuestro código fuente habilitamos el siguiente test y comprobamos que está _rojo_
* Repetimos hasta acabar la kata
